﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace KinectCam
{
    class ConnectWrapper : IDisposable
    {
        // 壓縮的格式與方法
        public enum CompressType { JPEG_Google_Code };
        //可取得的內部資料
        public enum Context
        {
            Name,             // 名字會在送出width、height大小後取得，且在取得後valid值才會轉為true，才可以開始傳送影像
            Valid,            // 目前是否需要傳送影像，需在設定都溝通好之後才會為true，可由遠端控制關閉
            Compress_Size,    // 圖片壓縮後的大小
            Compress_Time,    // 壓縮圖片所花的時間，此為毫秒
            Send_Time         // 傳送圖片所花的時間，此為毫秒
        };

        // http://cc-young.blogspot.tw/2012/08/c-cc-dll.html
        
        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr connect_init(int width, int height, CompressType comType);

        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void connect_free(ref IntPtr connect);
        
        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int connect_start(IntPtr connect, string ip, int tcp_port, int udp_port);

        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int connect_close(IntPtr connect);

        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int connect_sendBGRA(IntPtr connect, byte[] image);

        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int connect_get_context(IntPtr connect, Context context);

        [DllImport("connect.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int connect_sendMIDI(IntPtr connect, int command, int d1, int d2);

        private IntPtr connect;

        public ConnectWrapper(int width, int height, CompressType comType=CompressType.JPEG_Google_Code)
        {
            this.connect = connect_init(width, height, comType);
        }
        public void Dispose()
        {
            _dispose(true);
        }
        protected virtual void _dispose(bool b)
        {
            if (this.connect != IntPtr.Zero)
            {
                connect_free(ref this.connect);
                this.connect = IntPtr.Zero;
            }
            if (b)
            {
                GC.SuppressFinalize(this);
            }
        }
        ~ConnectWrapper()
        {
            _dispose(false);
        }
        public int start(string ip, int tcpPort, int udpPort)
        {
            return connect_start(connect, ip, tcpPort, udpPort);
        }
        public int close()
        {
            return connect_close(this.connect);
        }
        public int sendBGRA(byte[] image)
        {
            return connect_sendBGRA(this.connect, image);
        }
        public int getContext(Context context)
        {
            return connect_get_context(this.connect, context);
        }
        public int sendMIDI(int command, int d1, int d2)
        {
            return connect_sendMIDI(this.connect, command, d1, d2);
        }
    }
}
