﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Kinect;
using System.Drawing;
using System.Drawing.Imaging;

namespace KinectCam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KinectSensor myKinect;
        ConnectWrapper connect;
        System.Diagnostics.Stopwatch fpsT;
        int count = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (KinectSensor.KinectSensors.Count == 0)
            {
                MessageBox.Show("No Kinects detected", "Camera Viewer");
                Application.Current.Shutdown();
            }

            try
            {
                myKinect = KinectSensor.KinectSensors[0];

                myKinect.ColorStream.Enable();

                myKinect.Start();
            }
            catch
            {
                MessageBox.Show("Kinect initialise failed", "Camera Viewer");
                Application.Current.Shutdown();
            }

            connect = new ConnectWrapper(640, 480);
            connect.start("127.0.0.1", 1200, 1201);

            fpsT = System.Diagnostics.Stopwatch.StartNew();

            myKinect.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(myKinect_ColorFrameReady);
        }

        byte[] colorData = null;
        WriteableBitmap colorImageBitmap = null;

        void myKinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            long callTime = 0;
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame == null) return;

                if (colorData == null)
                    colorData = new byte[colorFrame.PixelDataLength];

                colorFrame.CopyPixelDataTo(colorData);

                if (writeableBitmap.IsChecked == true)
                {
                    if (colorImageBitmap == null)
                    {
                        this.colorImageBitmap = new WriteableBitmap(
                            colorFrame.Width,
                            colorFrame.Height,
                            96,  // DpiX
                            96,  // DpiY
                            PixelFormats.Bgr32,
                            null);
                    }

                    this.colorImageBitmap.WritePixels(
                        new Int32Rect(0, 0, colorFrame.Width, colorFrame.Height),
                        colorData, // video data
                        colorFrame.Width * colorFrame.BytesPerPixel, // stride,
                        0   // offset into the array - start at 0
                        );

                    kinectVideo.Source = colorImageBitmap;
                }
                else
                {
                    kinectVideo.Source = BitmapSource.Create(
                                        colorFrame.Width, colorFrame.Height, // image dimensions
                                        96, 96,  // resolution - 96 dpi for video frames
                                        PixelFormats.Bgr32, // video format
                                        null,               // palette - none
                                        colorData,          // video data
                                        colorFrame.Width * colorFrame.BytesPerPixel // stride
                                        );
                }
                int compressSize = connect.getContext(ConnectWrapper.Context.Compress_Size);
                int compressTime = connect.getContext(ConnectWrapper.Context.Compress_Time);
                int sendTime = connect.getContext(ConnectWrapper.Context.Send_Time);
                int name = connect.getContext(ConnectWrapper.Context.Name);
                int vaild = connect.getContext(ConnectWrapper.Context.Valid);

                fpsT.Stop();
                long fps = fpsT.ElapsedMilliseconds;
                fpsT = System.Diagnostics.Stopwatch.StartNew();
                Console.Write("name: ");
                Console.Write(name);
                Console.Write(" \tvalid: ");
                Console.Write(vaild);
                Console.Write(" \tcomTime: ");
                Console.Write(compressTime);
                Console.Write(" \tsendTime: ");
                Console.Write(sendTime);
                Console.Write(" \tcallTime: ");
                Console.Write(callTime);
                Console.Write(" \tcomSize: ");
                Console.Write(compressSize);
                Console.Write(" \tFPS: ");
                Console.Write(1000/fps);
                Console.Write(" \tcount: ");
                Console.WriteLine(count);
                System.Diagnostics.Stopwatch callT = System.Diagnostics.Stopwatch.StartNew();
                connect.sendBGRA(colorData);
                callT.Stop();
                callTime = callT.ElapsedMilliseconds;

                if (count == 200)
                {
                    connect.close();
                }
                //count++;
                connect.sendMIDI(0x80, 60, 80);
            }
        }
        static void OnProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("I'm out of here");
        }
    }
}
