1. 請把我所在的資料夾(Connect)放在專案底下(.sln旁邊)
2. 並在你的專案名上點選右鍵，選擇屬性，裡面要設定以下幾點路徑
	(1) 組態屬性->C/C++->一般->其他Include目錄，輸入 .\Connect\include
	(2) 組態屬性->連結器->一般->其他程式庫目錄，輸入 .\Connect\lib\x86
	(3) 組態屬性->連結器->輸入->其他相依性，增加 Connect.lib
	(4) 複製Connect\lib\x86\Connect.dll到你的執行檔(.exe)同目錄下
		(執行檔位置一般在專案目錄下的Debug資料夾)
	(註: 如果為x64的程式碼，請把上面的x86都改x64即可)
3. 程式碼使用說明
	※註: 在Connect\include\connect.h裡面有每個函數和參數的詳細說明
	(1) Include:
		#include <connect.h>
	(2) 加入libary
		#pragma comment(lib, "connect.lib")
	(3) 建構子:
		struct connect *connect = connect_init(int 寬, int 高, compressType 壓縮格式，預設為google code壓縮);
		如果成功回傳大於等於零的數，失敗為負數
	(4) 連線到遠端主機:
		connect_start(connect, char *遠端主機ip位置, int 遠端主機TCP port, int 遠端主機UDP port);
		(如果不使用UDP傳輸，請在UDP port的位置輸入負數)
		如果成功回傳大於等於零的數，失敗為負數
	(5) 傳送影像到遠端主機:
		connect_sendBGRA(connect, const char *影像);
		(如沒有連線到遠端主機，則會return 0)
	(6) 關閉連線:
		connect_close(connect);
	(7) 釋放記憶體:
		connect_free(&connect);
	(8) 取得壓縮所花時間:
		int compressTime = connect_get_context(connect, Compress_Time);
	(9) 取得傳送所花時間:
		int sendTime = connect_get_context(connect, Send_Time);
	(10)取得壓縮大小:
		int compressSize=connect_get_context(connect, Compress_Size);
	(11)範例:
		#include <connect.h>
		#pragma comment(lib, "connect.lib")
		struct connect *connect = connect_init(640, 480);
		
		int result = connect_start(connect, "140.115.53.102", 1200, 1201);
		byte *imageFrame = new byte(640 * 480 * 4);
		connect_sendBGRA(connect, imageFrame);
		int compressTime = connect_get_context(connect, Compress_Time);
		int sendTime = connect_get_context(connect, Send_Time);
		int compressSize=connect_get_context(connect, Compress_Size);

		connect_close(connect);
		connect_free(&connect);