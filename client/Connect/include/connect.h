#pragma once

#ifdef CONNECT_EXPORTS
#define CONNECT_API __declspec(dllexport) 
#else
#define CONNECT_API __declspec(dllimport) 
#endif

#define CONNECT_READ_BUFFER_SIZE 10

// 錯誤代碼
#define NAME_STILL_NOT_SET -1                 // 名字(編號)還未設定
#define OVERFLOW_RETURN_SIZE -2               // 原數值超過return可以表示的範圍
#define INPUT_CONNECT_NULL -3                 // 輸入的struct connect為NULL
#define INPUT_PARAM_ERROR -4                  // 輸入的參數有誤
#define ALLOC_SOCKET_ERROR -5                 // 宣告socket時失敗
#define CANNOT_CONNECT_TO_REMOTE_SERVER -6    // 不能連線到遠端主機
#define PROGRAM_ERROR -7                      // 程式有錯誤，進入到不應該的地方

// 壓縮的格式與方法
enum CompressType { JPEG_Google_Code };
//可取得的內部資料
enum Context {
	Name,             // 名字會在送出width、height大小後取得，且在取得後valid值才會轉為true，才可以開始傳送影像
	Valid,            // 目前是否需要傳送影像，需在設定都溝通好之後才會為true，可由遠端控制關閉
	Compress_Size,    // 圖片壓縮後的大小
	Compress_Time,    // 壓縮圖片所花的時間，此為毫秒
	Send_Time         // 傳送圖片所花的時間，此為毫秒
};

typedef struct connect connect_t;


#ifdef __cplusplus
extern "C" {
#endif
	// 宣告初始struct
	// 如果初始化正常則return struct connect
	// 否則return NULL
	// 錯誤的可能有: libary引入失敗、new失敗、libevent event new失敗
	CONNECT_API struct connect *connect_init(int width, int height, CompressType comType = JPEG_Google_Code);
	// 釋放struct connect
	// 正常程序會把struct connect變成NULL
	// 如果輸入的struct connect為NULL，會return INPUT_CONNECT_NULL錯誤
	// 切記需要先connect_close才可以使用此function
	CONNECT_API int connect_free(struct connect *&con);
	// 連線到遠端主機
	// ip為遠端主機的IP位置，為IPv4協定，輸入的形式為字串
	// tcp_port為遠端主機開啟的TCP Port，為主要的溝通的管道，建議輸入1200
	// udp_port為遠端主機開啟的UDP Port，主要用在傳輸影像，建議輸入1201
	// 也可以輸入-1表示不啟用UDP傳輸，這樣所有的資料都會藉由TCP傳輸
	// 如果需要每張圖片都一定要傳輸的情況下可以適用輸入-1強制都使用TCP傳輸
	// 如果遇到網路狀況較不佳的情況，強烈建議使用UDP傳輸，如傳輸的距離較遠，中間的媒介為Wi-Fi的時候
	// 否則會有強烈的延遲
	// UDP傳輸的缺點: 每次的傳輸輛不行超過65507，如果超過會自動不傳輸
	// 但是因為有經過壓縮，所以也不清楚會不會超過，可以藉由connect_get_context(struct connect, Compress_Size)查看壓縮完後的大小
	// 正常成功會return 1;
	// 錯誤會return錯一代碼，可能的錯誤有
	// INPUT_CONNECT_NULL              : 輸入的struct connect為NULL
	// INPUT_PARAM_ERROR               : 輸入的參數有誤
	// ALLOC_SOCKET_ERROR              : 宣告socket時失敗
	// CANNOT_CONNECT_TO_REMOTE_SERVER : 不能連線到遠端主機
	CONNECT_API int connect_start(struct connect *con, const char* ip, const int tcp_port, const int udp_port);
	// 關閉遠端連線
	// 並會終止connect的thread
	CONNECT_API int connect_close(struct connect *con);
	// 傳輸影像到遠端主機
	// 輸入的格式為BGRA，共有4通道，每通道大小為1 char，且A值因為經JPEG壓縮，所以不會去看其內容
	// 主要的花費在輸入影像的複製上
	// 會和thread lock到儲存影像的記憶體，如果thread正在交換，則會等待
	// 如果正常設定好傳送資料給後端thread處理時，return 1
	// 如果是struct connect為NULL、資料未溝通完全、valid為false則會return 0，且不做任何事就return
	CONNECT_API int connect_sendBGRA(struct connect *con, const unsigned char* image);
	// 取得內部的資料
	// 且依照enum Context輸入想要取得的資料
	CONNECT_API int connect_get_context(struct connect *con, Context context);
	// 傳送MIDI訊號讓server發出指定的聲音
	CONNECT_API int connect_sendMIDI(struct connect *con, int command, int d1, int d2);
#ifdef __cplusplus
}
#endif
